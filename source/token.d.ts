declare namespace drupal {

  export namespace Core {

    export interface Behaviors {

      tokenTree: Core.Behavior;

      tokenInsert: Core.Behavior;

    }

  }

  export interface DrupalSettings {

    tokenFocusedField?: HTMLTextAreaElement | HTMLInputElement;

  }

}
