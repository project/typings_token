(function (
  $: JQueryStatic,
  Drupal: drupal.DrupalStatic,
  drupalSettings: drupal.DrupalSettings
): void {

  'use strict';

  let element: HTMLElement;
  element = <HTMLTextAreaElement> drupalSettings.tokenFocusedField;
  element = <HTMLInputElement> drupalSettings.tokenFocusedField;

  let behavior: drupal.Core.Behavior;
  behavior = Drupal.behaviors.tokenInsert;
  behavior = Drupal.behaviors.tokenTree;

}(jQuery, Drupal, drupalSettings));
